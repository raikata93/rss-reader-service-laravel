# RSS reader

This is a service which send rss urls to rabbitMQ message broker and lister for rss feeds. All feeds are populated into tables with different functionality for searching and ordering 

#### Setup

1. Run `composer install` in order to setup the laravel app on your machine
1. Run `php artisan migrate` in order to create database tables
1. Run `docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3.11-management` in order to make a rabbitmq doker container
2. After that open http://localhost:15672 and log with guest username and guest password to rabbitMQ panel (those are the default credentials)
3. Run `php artisan serve` in order to open start the laravel service
4. After that run `php artisan consume:feeds` which is a custom command which listen for feeds
5. Open RSS reader service and run `go run main.go` in order to run the consumer and producer
6. Now we are ready for checking all existing functionality, just set into `RSS urls` page rss feed and wait to see the  result into `rss feeds` page

Martin Raychev Ivanov
