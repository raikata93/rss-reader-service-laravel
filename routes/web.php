<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'App\Http\Controllers\RssController@index');
Route::get('/paging', 'App\Http\Controllers\RssController@linkPaging');
Route::post('/rss-urls', 'App\Http\Controllers\RssController@rssUrlsFeeds');
Route::get('/rss-urls/{id}/delete', 'App\Http\Controllers\RssController@delete');

Route::get('/rss-feeds', 'App\Http\Controllers\RssController@rssFeeds');
Route::get('/rss-feeds/paging', 'App\Http\Controllers\RssController@rssFeedsPaging');


