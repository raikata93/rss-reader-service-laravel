<?php

namespace App\Http\Controllers;

use App\Models\Rss;
use Illuminate\Http\Request;
use Yajra\Datatables\DataTables;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RssController extends Controller
{
    public function index() {
        return view("rss_urls");
    }

    public function linkPaging() {
        $feeds = json_decode(Rss::all(), true);

        return DataTables::of($feeds)
            ->addColumn('url', function ($feed) {
                return $feed['url'];
            })->addColumn('actions', function ($feed) {
                return '<a class="btn btn-danger"  style="margin-right:5px" href="'.url('rss-urls/'.$feed['id'].'/delete').'">Изтриване</a>';
            })->rawColumns(['actions'])->make(true);
    }

    public function rssFeeds() {
        return view("rss_feeds");
    }

    public function rssFeedsPaging() {
        $feeds = json_decode(Rss::all(), true);
        $allItems = [];
        foreach ($feeds as $feed) {
            $items = json_decode($feed['parsed_feed'], true);
            foreach ($items as $item) {
                array_push($allItems, $item);
            }
        }
        return DataTables::of($allItems)
            ->addColumn('title', function ($item) {
                $url = '<a target="_blank" href="'.url($item['link']).'">'.$item['title'].'</a>';
                return $url;
            })->addColumn('source', function ($item) {
                return $item['source'];
            })->addColumn('source_url', function ($item) {
                return $item['source_url'];
            })->addColumn('link', function ($item) {
                return $item['link'];
            })->addColumn('publish_date', function ($item) {
                return $item['publish_date'];
            })->addColumn('description', function ($item) {
                return $item['description'];
            })->rawColumns(['title'])->make(true);
    }


    public function rssUrlsFeeds(Request $request) {
        $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
        $channel = $connection->channel();

        $channel->queue_declare('rss-urls', false, false, false, false);

        $msg = new AMQPMessage($request->rss_urls);
        $channel->basic_publish($msg, '', 'rss-urls');

        $channel->close();
        $connection->close();

        return redirect()->back();
    }

    public function delete($id) {
        Rss::where("id", $id)->delete();
        return redirect()->back();
    }
}
