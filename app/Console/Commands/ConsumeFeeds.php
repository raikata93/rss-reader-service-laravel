<?php

namespace App\Console\Commands;

use App\Models\Rss;
use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class ConsumeFeeds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'consume:feeds';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Consume already parsed RSS feeds';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
        $channel = $connection->channel();

        $channel->queue_declare('rss-data', false, false, false, false);

        echo " [*] Waiting for messages. To exit press CTRL+C\n";

        $callback = function ($msg) {
            $feeds = json_decode($msg->body, true);
            $rss = new Rss();
            $rss->parsed_feed = json_encode($feeds["items"], JSON_UNESCAPED_UNICODE);
            $rss->url = $feeds["items"][0]["source"];
            $rss->save();
        };

        $channel->basic_consume('rss-data', '', false, true, false, false, $callback);
        while ($channel->is_open()) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();

        return Command::SUCCESS;
    }
}
